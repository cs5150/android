package edu.cornell.svms;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import edu.cornell.svms.model.Student;

@Controller    
@RequestMapping(path="/student") 
public class MainController {
	
	@RequestMapping(value = { "/" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET }, produces = { "application/json; charset=utf-8" })
	@ResponseBody
	public Student getAllUsers() {
		Student s = new Student();
		s.setFirstName("Ron");
		s.setLastName("Mayer");
		s.setPrimaryContact(9866969);
		return s;
	}

}
