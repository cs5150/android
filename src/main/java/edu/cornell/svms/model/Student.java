package edu.cornell.svms.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.cornell.svms.enumeration.BusStop;

@Entity
@Table(name = "student")
public class Student {
	
	@Id
	private Integer id;
	
	@Column(name="register_no", unique= true)
	private Integer registerNo;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="dob")
	private Date dob;
	
//	@Column(name="grade")
//	private Grade grade;
	
	@Column(name="primary_contact")
	private Integer primaryContact;
	
	@Column(name="secondary_contact")
	private Integer secondaryContact;
	
	@Column(name="address")
	private String address; // comma separated string of Door no, Street, Village and Pin code
	
	@Column(name="bus_stop")
	private BusStop busStop;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRegisterNo() {
		return registerNo;
	}

	public void setRegisterNo(Integer registerNo) {
		this.registerNo = registerNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

//	public Grade getGrade() {
//		return grade;
//	}
//
//	public void setGrade(Grade grade) {
//		this.grade = grade;
//	}

	public Integer getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(Integer primaryContact) {
		this.primaryContact = primaryContact;
	}

	public Integer getSecondaryContact() {
		return secondaryContact;
	}

	public void setSecondaryContact(Integer secondaryContact) {
		this.secondaryContact = secondaryContact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BusStop getBusStop() {
		return busStop;
	}

	public void setBusStop(BusStop busStop) {
		this.busStop = busStop;
	}
	
	
}
