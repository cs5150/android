package edu.cornell.svms.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.cornell.svms.enumeration.BusStop;

@Entity
@Table(name = "teacher")
public class Teacher {
	
	@Id
	private Integer id;
	
	@Column(name="emp_no", unique= true)
	private Integer empNo;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="contact")
	private Integer contact;
	
	@Column(name="address")
	private String address; // comma separated string of Door no, Street, Village and Pin code
	
	@Column(name="bus_stop")
	private BusStop busStop;
	
	@Column(name="dob")
	private Date dob;
	
	//TODO: Define the relationship bw teacher, subject & class

}
