package com.example.jc.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by JC on 3/1/18.
 */

public class Student implements Serializable {
    private String id;
    private String name;
    private Standard standard;
    private String address;
    private String contact;
    private String classTeacher;
    private String schoolAddress;
    private String schoolContact;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getClassTeacher() {
        return classTeacher;
    }

    public void setClassTeacher(String classTeacher) {
        this.classTeacher = classTeacher;
    }

    public String getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchoolContact() {
        return schoolContact;
    }

    public void setSchoolContact(String schoolContact) {
        this.schoolContact = schoolContact;
    }



    public String getName() {
        return name;
    }

    public Standard getStandard() {
        return standard;
    }

    public void setStandard(Standard standard) {
        this.standard = standard;
    }

    public void setName(String name) {
        this.name = name;
    }

}
