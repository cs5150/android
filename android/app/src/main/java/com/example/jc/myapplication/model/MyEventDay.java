package com.example.jc.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.applandeo.materialcalendarview.EventDay;

import java.util.Calendar;

/**
 * Created by JC on 3/4/18.
 */

public class MyEventDay extends EventDay implements Parcelable {

    public String mNote;
    public Calendar day;

    public MyEventDay(Calendar day, int imageResource, String note) {
        super(day, imageResource);
        this.day = day;
        mNote = note;
    }
    public String getNote() {
        return mNote;
    }
    public Calendar getCalendar(){return day;}
    private MyEventDay(Parcel in) {
        super((Calendar) in.readSerializable(), in.readInt());
        mNote = in.readString();
    }
    public static final Creator<MyEventDay> CREATOR = new Creator<MyEventDay>() {
        @Override
        public MyEventDay createFromParcel(Parcel in) {
            return new MyEventDay(in);
        }
        @Override
        public MyEventDay[] newArray(int size) {
            return new MyEventDay[size];
        }
    };
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(getCalendar());
        parcel.writeInt(getImageResource());
        parcel.writeString(mNote);
    }
    @Override
    public int describeContents() {
        return 0;
    }
}