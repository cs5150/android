package com.example.jc.myapplication.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Map;

/**
 * Created by apple on 3/24/18.
 */

public class CourseTeacher implements Serializable {
    private Standard standard;
    private String subject;

    public Standard getStandard() {
        return standard;
    }

    public void setStandard(Standard standard) {
        this.standard = standard;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
