package com.example.jc.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jc.myapplication.model.HomeworkParent;


import java.util.ArrayList;

public class HomeworkAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<HomeworkParent> homeworkArray;
    private LayoutInflater mInflater;

    public HomeworkAdapter(Context context, ArrayList<HomeworkParent> homeworkArray) {
        this.context = context;
        this.homeworkArray = homeworkArray;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override

    public int getCount() {
        return homeworkArray.size();
    }

    @Override
    public Object getItem(int position) {
        return homeworkArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HomeworkParent homework = (HomeworkParent) getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.home_list_item, parent, false);
        }

        TextView subject = (TextView) convertView.findViewById(R.id.eventDescription);
        TextView description = (TextView) convertView.findViewById(R.id.description);

        subject.setText(homework.getSubject());
        description.setText(homework.getDescription());

        return convertView;
    }
}
