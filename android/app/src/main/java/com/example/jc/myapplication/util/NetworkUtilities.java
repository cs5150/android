package com.example.jc.myapplication.util;

import android.util.Log;

import com.example.jc.myapplication.model.OnLoadTeacher;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by JC on 2/24/18.
 */

public final class NetworkUtilities {
    private static final String TAG = NetworkUtilities.class.getSimpleName();
    private static final String BASE_URL = "http://52.168.170.218:8080/svms/";

    public static URL buildURL(String path){

        try{
            String specificURL = BASE_URL+path+"/";
            URL queryUrl = new URL(specificURL);
            return queryUrl;
        }catch (MalformedURLException e){
            e.printStackTrace();
            Log.i("tag","not even parsing url");
            return null;
        }

    }

    public static String getObjectType(URL url){
        String urlString = url.toString();
        String[] paths = urlString.split("/");
        return paths[paths.length-1];
    }

    public static String getResponseFromHttp(URL url){

        HttpURLConnection urlConnection = null;

        try{

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

            if(urlConnection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
                InputStream in = urlConnection.getInputStream();
                InputStreamReader read = new InputStreamReader(in);

                BufferedReader reader = new BufferedReader(read);

                StringBuffer buf = new StringBuffer("");
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buf.append(line);
                }
                String json = buf.toString();
                reader.close();
                return json;
            }else {
                return null;
            }

        }
        catch (IOException e){
            Log.i(TAG,"IO EXCEPTION");
            e.printStackTrace();
            return null;

        }

    }

    public static boolean postMarks(URL url, OnLoadTeacher onload){
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            urlConnection.setConnectTimeout(30000);
            urlConnection.setDoOutput(true);


            Gson gson = new Gson();
            String json = gson.toJson(onload);


            JSONObject post = new JSONObject(json);



            DataOutputStream os = new DataOutputStream(urlConnection.getOutputStream());
            os.writeBytes(post.toString());
            os.flush();
            os.close();

            int responseCode=urlConnection.getResponseCode();
            return true;




        }catch(Exception e){
            e.printStackTrace();
            return false;
        }

    }

    public static boolean postHomeworkToServer(URL url, String description, Date date){

        HttpURLConnection urlConnection = null;
        SimpleDateFormat ft =
                new SimpleDateFormat ("yyyy-MM-dd");

        try{
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            urlConnection.setConnectTimeout(30000);
            urlConnection.setDoOutput(true);

            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", 6);
            postDataParams.put("courseId", 1);
            postDataParams.put("description", description);
            postDataParams.put("date", ft.format(date));

            DataOutputStream os = new DataOutputStream(urlConnection.getOutputStream());
            os.writeBytes("["+postDataParams.toString()+"]");
            os.flush();
            os.close();

            int responseCode=urlConnection.getResponseCode();
            Log.i("tag",""+responseCode);

            return true;
        }catch(IOException e){
            e.printStackTrace();
            Log.i(TAG,"IOEXCEPT");
            return false;
        }
        catch(JSONException e){
            e.printStackTrace();
            Log.i(TAG,"JSONEXCEPT");

            return false;

        }

        finally{
            urlConnection.disconnect();
        }

    }

    public static boolean updateStudentAbsence(URL url, ArrayList<Integer> ids){
        Date date = new Date();
        SimpleDateFormat ft =
                new SimpleDateFormat ("yyyy-MM-dd");

        HttpURLConnection urlConnection = null;

        try{

            for (int id : ids){
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setConnectTimeout(30000);
                urlConnection.setDoOutput(true);

                DataOutputStream os = new DataOutputStream(urlConnection.getOutputStream());
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("studentId", id);
                postDataParams.put("date", ft.format(date));
                os.writeBytes("["+postDataParams.toString()+"]");
                os.flush();
                os.close();
                int responseCode = urlConnection.getResponseCode();
                urlConnection.getResponseMessage();
                Log.i("tag",""+responseCode);
            }

            return true;
        }catch(IOException e){
            e.printStackTrace();
            Log.i(TAG,"IOEXCEPT");
            return false;
        }
        catch(JSONException e){
            e.printStackTrace();
            Log.i(TAG,"JSONEXCEPT");

            return false;

        }

        finally{
            urlConnection.disconnect();
        }


    }
}

