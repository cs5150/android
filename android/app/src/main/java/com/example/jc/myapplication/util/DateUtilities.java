package com.example.jc.myapplication.util;

/**
 * Created by JC on 3/4/18.
 */

public final class DateUtilities {
    public static int[] getYearMonthDay(String date){
        String[] parts = date.split("-");
        int[] result = new int[3];
        int count =0;
        for(String part : parts){
            result[count++] = Integer.valueOf(part);
        }
        return result;
    }


}
