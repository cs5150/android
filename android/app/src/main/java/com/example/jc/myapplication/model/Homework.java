package com.example.jc.myapplication.model;

import java.io.Serializable;

/**
 * Created by apple on 3/3/18.
 */

public class Homework implements Serializable {
    //private Course course;
    private String description;
    private int id;
    private int courseId;
    private String date;

    public Homework(int id, String description, int courseId, String date) {
        //this.course = course;
        this.description = description;
        this.id = id;
        this.courseId = courseId;
        this.date = date;
    }

    /*public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }*/

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
