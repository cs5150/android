package com.example.jc.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AnnouncementDesAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> announcements;
    private LayoutInflater mInflater;


    public AnnouncementDesAdapter(Context context, ArrayList<String> announcements) {
        this.context = context;
        this.announcements = announcements;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override

    public int getCount() {
        return announcements.size();
    }

    @Override
    public Object getItem(int position) {
        return announcements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String announcement = (String)getItem(position);


        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.announce_des_list_item, parent, false);
        }

        TextView listView = (TextView) convertView.findViewById(R.id.eventDescription);
        listView.setText(announcement);

        return convertView;
    }
}
