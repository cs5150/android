package com.example.jc.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jc.myapplication.model.HomeworkParent;
import com.example.jc.myapplication.model.OnLoadTeacher;
import com.example.jc.myapplication.model.Teacher;
import com.example.jc.myapplication.util.NetworkUtilities;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.asList;

public class AddHomeworkActivity extends AppCompatActivity {

    ArrayList<String> names;

    String course;
    Teacher teacher;
    Map<String, Map<String, ArrayList<HomeworkParent>>> homework;
    Map<String, ArrayList<String>> announcements;
    String teacherId;
    Map<String, Map<String, Map<String, Integer>>> marks;
    Intent intent;

    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_homework);

        final TextView hwTitle = (TextView) findViewById(R.id.hw_name_field);

        final DatePicker hwDate = (DatePicker) findViewById(R.id.hw_date_picker);

        intent = getIntent();
        if(intent.hasExtra("isEdit")) {
            String date = intent.getStringExtra("date");
            try {
                Date date1 = dateFormatter.parse(date);
                hwDate.updateDate(Integer.parseInt(DateFormat.format("yyyy", date1).toString()),
                        Integer.parseInt(DateFormat.format("MM", date1).toString()) - 1,
                        Integer.parseInt(DateFormat.format("dd", date1).toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            hwTitle.setText(intent.getStringExtra("desc"));
        }
        loadPreferences();

        Button submitButton = (Button) findViewById(R.id.hw_submit_button);
        submitButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Date date = getDateAndUpdate(hwDate);
                String dateString = dateFormatter.format(date);

                HomeworkParent newhw = new HomeworkParent();
                newhw.setDescription(hwTitle.getText().toString());
                newhw.setSubject(null);

                Map<String, ArrayList<HomeworkParent>> outer = homework.get(intent.getStringExtra("class"));
                if (outer == null){
                    Map<String, ArrayList<HomeworkParent>> inner = new HashMap<String, ArrayList<HomeworkParent>>();
                    inner.put(dateString, new ArrayList<>(asList(newhw)));;
                    homework.put(intent.getStringExtra("classes"), inner);

                }
                else {
                    ArrayList<HomeworkParent> hwList = outer.get(date.toString());
                    if (hwList == null){
                        outer.put(dateString, new ArrayList<>(asList(newhw)));
                    }
                    else {
                        hwList.add(newhw);
                    }
                }

                Intent data = new Intent();
                data.putExtra("date", dateString);
                data.putExtra("desc", hwTitle.getText().toString());
                new ProcessJson().execute("");
                setResult(RESULT_OK, data);
                finish();
            }
//
//            @Override
//            public void onClick(View v) {
//                Thread thread = new Thread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        try  {
//                            NetworkUtilities.postHomeworkToServer(NetworkUtilities.buildURL("homework"), hwTitle.getText().toString(), getDateFromDatePicker(hwDate));
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                thread.start();
//                finish();
//            }

        });

    }

    public void loadPreferences() {
        SharedPreferences pref = getSharedPreferences("onLoadTeacher", MODE_PRIVATE);
        String teacherMarks = pref.getString("teacherMarks", "");
        String teacherHomework = pref.getString("teacherHomework","");
        String teacherAnnouncements = pref.getString("teacherAnnouncements","");
        String teacherJson = pref.getString("teacher","");

        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, Map<String, Map<String, Integer>>>>(){}.getType();
        Type type2 = new TypeToken<Map<String, Map<String, ArrayList<HomeworkParent>>>>(){}.getType();
        Type type3 = new TypeToken<Map<String, ArrayList<String>>>(){}.getType();
        teacherId = pref.getString("teacherId","");
        marks = gson.fromJson(teacherMarks, type);
        homework = gson.fromJson(teacherHomework,type2);
        announcements = gson.fromJson(teacherAnnouncements,type3);
        teacher = gson.fromJson(teacherJson,Teacher.class);
    }

    public java.util.Date getDateAndUpdate(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        System.out.println(day);
        return calendar.getTime();
    }

    public class ProcessJson extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = NetworkUtilities.buildURL("load/teacher/"+teacherId);
            OnLoadTeacher onLoadTeacher = new OnLoadTeacher();
            onLoadTeacher.setId(teacherId);
            onLoadTeacher.setMarks(marks);
            onLoadTeacher.setAnnouncements(announcements);
            onLoadTeacher.setTeacher(teacher);
            onLoadTeacher.setHomework(homework);
            boolean what = NetworkUtilities.postMarks(url, onLoadTeacher);
            Log.i("test",""+what);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}
