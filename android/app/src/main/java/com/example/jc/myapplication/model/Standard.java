package com.example.jc.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by apple on 3/3/18.
 */

public class Standard implements Serializable {
    private int id;
    private String section;
    private String standard;

    public Standard(int id, String section, String standard) {
        this.id = id;
        this.section = section;
        this.standard = standard;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }
}
