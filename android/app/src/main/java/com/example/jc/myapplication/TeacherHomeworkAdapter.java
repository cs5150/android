package com.example.jc.myapplication;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jc.myapplication.model.HomeworkParent;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by jingqi on 5/7/18.
 */

public class TeacherHomeworkAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater layoutInflater;
    private ArrayList<Pair<String, String>> pairsList;

    public TeacherHomeworkAdapter (Context context, ArrayList<Pair<String, String>> pairsList){
        this.mContext = context;
        this.pairsList = pairsList;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pairsList.size();
    }

    @Override
    public Object getItem(int position) {
        return pairsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        Pair<String, String> pair = (Pair<String, String>) getItem(position);
        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.teacher_homework_detail, parent, false);

            holder = new ViewHolder();
            holder.delete = convertView.findViewById(R.id.delete_homework_button);
            holder.edit = convertView.findViewById(R.id.edit_homework_button);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        TextView dateText = (TextView) convertView.findViewById(R.id.hw_date_text);
        dateText.setText(pair.first);
        TextView hwDesc = (TextView) convertView.findViewById(R.id.hw_item_description);
        hwDesc.setText(pair.second);


//        Button edit = convertView.findViewById(R.id.edit_homework_button);
//        edit.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                System.out.println("hello");
//
//            }
//        });
//
//        Button delete = convertView.findViewById(R.id.delete_homework_button);
//        delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                System.out.println("Bye");
//            }
//        });

//        final Button editButton = holder.edit;
//        editButton.setTag(position);
//        editButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                if (mContext instanceof TeacherHomeworkActivity){
//                    ((TeacherHomeworkActivity)mContext).editHw((Integer) editButton.getTag());
//                }
//            }
//        });

        final Button deleteButton = holder.delete;
        deleteButton.setTag(position);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof TeacherHomeworkActivity){
                    ((TeacherHomeworkActivity)mContext).removeHw((Integer) deleteButton.getTag());
                }
            }
        });

        return convertView;
    }

    private static class ViewHolder{
        //        public TextView listItem;
        public Button edit;
        public Button delete;
    }
}
