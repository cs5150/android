package com.example.jc.myapplication.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

/**
 * Created by apple on 3/24/18.
 */

public class Announcement implements Serializable {
    private Date date;
    private ArrayList<String> description;

    public Announcement(Date date, ArrayList<String> description) {
        this.date = date;
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<String> getDescription() {
        return description;
    }

    public void setDescription(ArrayList<String> description) {
        this.description = description;
    }
}
