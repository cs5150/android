package com.example.jc.myapplication.model;

import java.io.Serializable;

/**
 * Created by apple on 3/24/18.
 */

public class SubjectMarks implements Serializable {
    String subject;
    int marks;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }
}
