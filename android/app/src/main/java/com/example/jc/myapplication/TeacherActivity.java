package com.example.jc.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jc.myapplication.model.OnLoadTeacher;
import com.example.jc.myapplication.model.Student;
import com.example.jc.myapplication.model.Teacher;
import com.example.jc.myapplication.util.JsonUtilities;
import com.example.jc.myapplication.util.NetworkUtilities;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.URL;

public class TeacherActivity extends AppCompatActivity {

    Button mHomeworkButton;
    Button mMarksButton;
    Button mAttendanceButton;
    Button mAnnouncementsButton;
    TextView nameText;
    TextView numberText;
    SharedPreferences sharedPreferences;
    SharedPreferences sharedPreferences1;
    String teacherAdNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);

        mHomeworkButton = findViewById(R.id.homeworkButton);
        mMarksButton = findViewById(R.id.marksButton);
        nameText = findViewById(R.id.nameText);
        numberText = findViewById(R.id.numberText);
        teacherAdNum = getIntent().getExtras().getString("teacherAdNum");
        sharedPreferences = getSharedPreferences("onLoadTeacher", Context.MODE_PRIVATE);
        sharedPreferences1 = getSharedPreferences("onLoadStudent", Context.MODE_PRIVATE);

        new ProcessJson().execute("");

    }

    public void onClick(View view){

        Intent intent;


        switch (view.getId()){

            case R.id.homeworkButton:

                intent = new Intent(this, ClassListActivity.class);
                intent.putExtra("type","homework");
                intent.putExtra("teacherAdNum",teacherAdNum);
                startActivity(intent);
                break;

            case R.id.marksButton:
                intent = new Intent(this, ClassListActivity.class);
                intent.putExtra("type","marks");
                intent.putExtra("teacherAdNum",teacherAdNum);
                startActivity(intent);
                break;
            case R.id.announcementButton1:
                intent = new Intent(this, AnnouncementActivity.class);
                startActivity(intent);
                break;

        }
    }

    public class ProcessJson extends AsyncTask<String, Void, Teacher> {
        private String json3;
        private OnLoadTeacher onLoadTeacher;

        @Override
        protected Teacher doInBackground(String... params) {
            URL url = NetworkUtilities.buildURL("load/teacher/"+teacherAdNum);
            String json = NetworkUtilities.getResponseFromHttp(url);

            Teacher teacher = JsonUtilities.saveTeacherData(json,sharedPreferences);

            return teacher;

        }

        @Override
        protected void onPostExecute(Teacher result) {
            super.onPostExecute(result);
            nameText.setText(result.getName());
            numberText.setText(teacherAdNum);

        }
    }
}
