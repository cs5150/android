package com.example.jc.myapplication;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jc.myapplication.model.HomeworkParent;
import com.example.jc.myapplication.model.OnLoadTeacher;
import com.example.jc.myapplication.model.Teacher;
import com.example.jc.myapplication.util.NetworkUtilities;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TeacherExam extends AppCompatActivity {

    TextView title;
    String examType;
    RelativeLayout ll;
    int editId;
    Map<String, Map<String, Map<String, Integer>>> map;
    ArrayList<String> names;
    String course;
    Teacher teacher;
    Map<String, Map<String, ArrayList<HomeworkParent>>> homework;
    Map<String, ArrayList<String>> announcements;
    String teacherId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_exam);


        ll = findViewById(R.id.relative);
        title = findViewById(R.id.title);
        examType = getIntent().getExtras().getString("examType");

        SharedPreferences pref = getSharedPreferences("onLoadTeacher", MODE_PRIVATE);
        String teacherMarks = pref.getString("teacherMarks", "");
        String teacherHomework = pref.getString("teacherHomework","");
        String teacherAnnouncements = pref.getString("teacherAnnouncements","");
        String teacherJson = pref.getString("teacher","");
        teacherId = pref.getString("teacherId","");

        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, Map<String, Map<String, Integer>>>>(){}.getType();
        Type type2 = new TypeToken<Map<String, Map<String, ArrayList<HomeworkParent>>>>(){}.getType();
        Type type3 = new TypeToken<Map<String, ArrayList<String>>>(){}.getType();
        map = gson.fromJson(teacherMarks, type);
        homework = gson.fromJson(teacherHomework,type2);
        announcements = gson.fromJson(teacherAnnouncements,type3);
        teacher = gson.fromJson(teacherJson,Teacher.class);







        Log.i("test",examType);
        title.setText(examType);

        int count = 40;
        names = new ArrayList<>();
        editId = 0;
        for(Map.Entry<String, Map<String, Map<String, Integer>>>entry: map.entrySet()){
            Log.i("test",entry.getKey());
            course = entry.getKey();

            if(course.equals("English,6A")) {

                for (Map.Entry<String, Integer> entry2 : entry.getValue().get(examType).entrySet()) {
                    Log.i("test", entry2.getKey());
                    TextView text = new TextView(this);
                    String[] key = entry2.getKey().split(",");
                    text.setText(key[0]);
                    names.add(entry2.getKey());

                    EditText edit = new EditText(this);
                    edit.setId(editId);

                    text.setX(60);
                    text.setTextSize(16);
                    text.setY(70 + count);

                    edit.setX(230);
                    edit.setTextSize(16);
                    edit.setY(30 + count);
                    ll.addView(text);
                    ll.addView(edit);
                    editId++;
                    count += 80;
                }
                break;
            }
        }

        Button button = new Button(this);
        button.setX(50);
        button.setY(count+240);
        button.setText("POST");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String,Integer> update = new HashMap<>();

                for(int i=0; i<editId;i++){
                    EditText edit = (EditText)findViewById(i);
                    int grade = Integer.parseInt(edit.getText().toString());
                    Log.i("test",names.get(i)+" grade : "+grade);
                    update.put(names.get(i),grade);


                }
                map.get(course).put(examType,update);
                Log.i("test",map.get(course).get(examType).toString());
                new ProcessJson().execute("");

                Toast.makeText(getApplicationContext(),"Grade has been posted!",Toast.LENGTH_SHORT).show();


            }
        });
        ll.addView(button);




    }

    public class ProcessJson extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = NetworkUtilities.buildURL("load/teacher/"+teacherId);
            OnLoadTeacher onLoadTeacher = new OnLoadTeacher();
            onLoadTeacher.setId(teacherId);
            onLoadTeacher.setMarks(map);
            onLoadTeacher.setAnnouncements(announcements);
            onLoadTeacher.setTeacher(teacher);
            onLoadTeacher.setHomework(homework);
            boolean what = NetworkUtilities.postMarks(url, onLoadTeacher);
            Log.i("test",""+what);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}
