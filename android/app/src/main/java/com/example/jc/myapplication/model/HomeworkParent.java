package com.example.jc.myapplication.model;

import java.io.Serializable;

/**
 * Created by apple on 3/24/18.
 */

public class HomeworkParent implements Serializable {
    private String description;
    private String subject;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

}
