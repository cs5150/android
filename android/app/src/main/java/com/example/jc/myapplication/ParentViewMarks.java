package com.example.jc.myapplication;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.*;
import java.util.Map;
import java.util.*;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.Context;
import android.widget.TextView;
import java.util.Random;

import com.example.jc.myapplication.model.HomeworkParent;
import com.example.jc.myapplication.model.OnLoadStudent;
import com.example.jc.myapplication.util.JsonUtilities;
import com.example.jc.myapplication.util.NetworkUtilities;
import com.example.jc.myapplication.model.Student;
import java.net.URL;

// ***** TO DO *****
// GET STUDENT ID
// RETRIVE DATA FROM JSON

public class ParentViewMarks extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    Bundle bundle;
    private Map<String, Map<String, Integer>> marks;  // first key is exam type, second key is subject name
    public Map<String, Map<String, Integer>> getMarks() {
        return marks;
    }


    private ListView mListView;

    ArrayList<List<String>> marksArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_view_marks);
        sharedPreferences = getSharedPreferences("onLoadStudent", Context.MODE_PRIVATE);
        String json = sharedPreferences.getString("marks", "");
        marks = JsonUtilities.parseMarksFromJson(json);
        Intent mIntent = getIntent();
        bundle = getIntent().getExtras();
        String value = bundle.getString("value");
        marksArray = new ArrayList<>();
        int sum = 0;

        for (Map.Entry<String, Integer> e : marks.get(value).entrySet()) {
            List<String> list = new ArrayList<>();
            list.add(e.getKey());
            sum += e.getValue();
            list.add(Integer.toString(e.getValue()));
            marksArray.add(list);
        }


        List<String> list = new ArrayList<>();
        list.add("Total");
        list.add(Integer.toString(sum));
        marksArray.add(list);


        mListView = (ListView) findViewById(R.id.marks_list_view);

        final MarksAdapter adapter = new MarksAdapter(this, marksArray);

        mListView.setAdapter(adapter);


    }

}
