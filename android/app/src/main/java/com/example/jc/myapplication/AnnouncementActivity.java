package com.example.jc.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.jc.myapplication.model.Announcement;
import com.example.jc.myapplication.model.HomeworkParent;
import com.example.jc.myapplication.util.JsonUtilities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Map;

public class AnnouncementActivity extends AppCompatActivity {

    ListView mListView;

    ListView mListViewDes;

    private Map<Date, ArrayList<String>> announcements;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);
        setTitle("Announcements");

        mListView = (ListView) findViewById(R.id.announce_list_view);
        mListViewDes = (ListView) findViewById(R.id.descriptionList);
        SharedPreferences mPrefs = getSharedPreferences("onLoadStudent",MODE_PRIVATE);
        String json = mPrefs.getString("announcements", "");
        ArrayList<Announcement> announcementArray = new ArrayList<>();
        announcements = JsonUtilities.parseAnnouncementFromJson(json);
        for (Map.Entry<Date, ArrayList<String>> entry : announcements.entrySet()) {
            Announcement announcement = new Announcement(entry.getKey(), entry.getValue());
            announcementArray.add(announcement);
        }

        final AnnouncementAdapter adapter = new AnnouncementAdapter(this, announcementArray);

        mListView.setAdapter(adapter);
    }
}
