package com.example.jc.myapplication.model;

import java.io.Serializable;

/**
 * Created by apple on 3/3/18.
 */

public class HomeworkDescriptor implements Serializable {
    private DailyHomework homework;

    public HomeworkDescriptor(DailyHomework homework) {
        this.homework = homework;
    }

    public DailyHomework getHomework() {
        return homework;
    }

    public void setHomework(DailyHomework homework) {
        this.homework = homework;
    }
}
