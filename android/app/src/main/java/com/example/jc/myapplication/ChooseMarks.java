package com.example.jc.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;



public class ChooseMarks extends AppCompatActivity implements View.OnClickListener{

    Bundle bundle, bundleID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_marks);

        Button MidButton1 = (Button) findViewById(R.id.MidButton1);
        MidButton1.setOnClickListener(this);
        Button QuarButton = (Button) findViewById(R.id.QuarButton);
        QuarButton.setOnClickListener(this);
        Button MidButton2 = (Button) findViewById(R.id.MidButton2);
        MidButton2.setOnClickListener(this);
        Button HalfYearlyButton = (Button) findViewById(R.id.HalfYearlyButton);
        HalfYearlyButton.setOnClickListener(this);
        Button MidButton3 = (Button) findViewById(R.id.MidButton3);
        MidButton3.setOnClickListener(this);
        Button AnnualButton = (Button) findViewById(R.id.AnnualButton);
        AnnualButton.setOnClickListener(this);
    }

    public void onClick(View v) {

        bundle = new Bundle();
        bundleID = new Bundle();
        //bundleID = getIntent().getExtras();
        Intent intent = new Intent(ChooseMarks.this, ParentViewMarks.class);
        switch (v.getId()) {

            case R.id.MidButton1:
                bundle.putString("value", "MIDTERM 1");
                //bundle.putString("studentID", bundleID.getString("studentID"));
                intent.putExtras(bundle);
                //intent.putExtras(bundleID);
                break;

            case R.id.QuarButton:
                bundle.putString("value", "QUARTERLY");
                intent.putExtras(bundle);
                break;

            case R.id.MidButton2:
                bundle.putString("value", "MIDTERM 2");
                intent.putExtras(bundle);
                break;
            case R.id.HalfYearlyButton:
                bundle.putString("value", "HALF YEARLY");
                intent.putExtras(bundle);
                break;
            case R.id.MidButton3:
                bundle.putString("value", "MIDTERM 3");
                intent.putExtras(bundle);
                break;
            case R.id.AnnualButton:
                bundle.putString("value", "ANNUAL");
                intent.putExtras(bundle);
                break;
        }

        startActivity(intent);
    }


}
