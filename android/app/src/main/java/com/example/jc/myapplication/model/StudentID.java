package com.example.jc.myapplication.model;

import java.io.Serializable;

/**
 * Created by JC on 4/11/18.
 */

public class StudentID implements Serializable{

    String name;
    String id;

    public StudentID(String name, String id){
        this.name= name;
        this.id= id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StudentID [name=");
        builder.append(name);
        builder.append(", id=");
        builder.append(id);
        builder.append("]");
        return builder.toString();
    }

}
