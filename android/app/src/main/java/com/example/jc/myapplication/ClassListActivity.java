package com.example.jc.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

public class ClassListActivity extends AppCompatActivity {
    String teachertype;
    Long teacherAdNum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_homework);
        teachertype = getIntent().getExtras().getString("type");
        teacherAdNum = Long.parseLong(getIntent().getExtras().getString("teacherAdNum"));

        SharedPreferences pref = getSharedPreferences("onLoadTeacher", MODE_PRIVATE);
        String teacherMarks = pref.getString("teacherMarks", "");
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, Map<String, Map<String, Integer>>>>(){}.getType();
        Map<String, Map<String, Map<String, Integer>>> map = gson.fromJson(teacherMarks, type);


        final ArrayList<String> classes = new ArrayList<>();

        for(Map.Entry<String, Map<String, Map<String, Integer>>>entry: map.entrySet()){
            classes.add(entry.getKey());
        }





        ListView classView = (ListView) findViewById(R.id.teacher_classlist);
        final ArrayAdapter<String> classAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.class_row_item, classes);
        classView.setAdapter(classAdapter);

        classView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(teachertype.equals("homework")) {
                    Intent intent = new Intent(getApplicationContext(), TeacherHomeworkActivity.class);

                    Log.i("test",classAdapter.getItem(position));
                    intent.putExtra("class", classAdapter.getItem(position));
                    startActivity(intent);
                }
                else if(teachertype.equals("marks")){
                    Intent intent = new Intent(getApplicationContext(), ExamTypeActivity.class);
                    Log.i("test",classAdapter.getItem(position));

                    intent.putExtra("class", classAdapter.getItem(position));

                    startActivity(intent);
                }
            }
        });


    }






}
