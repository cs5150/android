package com.example.jc.myapplication.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by apple on 3/24/18.
 */

public class OnLoadStudent implements Serializable {
    private String id;
    private Standard standard;
    private ArrayList<String> subjectNames;
    private Map<Date, ArrayList<HomeworkParent>> homework;
    private Map<String, Map<String, Integer>> marks;  // first key is exam type, second key is subject name
    private Map<Date, ArrayList<String>> announcements;
    private Student student;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Standard getStandard() {
        return standard;
    }

    public void setStandard(Standard standard) {
        this.standard = standard;
    }

    public ArrayList<String> getSubjectNames() {
        return subjectNames;
    }

    public void setSubjectNames(ArrayList<String> subjectNames) {
        this.subjectNames = subjectNames;
    }

    public Map<Date, ArrayList<HomeworkParent>> getHomework() {
        return homework;
    }

    public void setHomework(Map<Date, ArrayList<HomeworkParent>> homework) {
        this.homework = homework;
    }

    public Map<String, Map<String, Integer>> getMarks() {
        return marks;
    }

    public void setMarks(Map<String, Map<String, Integer>> marks) {
        this.marks = marks;
    }

    public Map<Date, ArrayList<String>> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(Map<Date, ArrayList<String>> announcements) {
        this.announcements = announcements;
    }
}
