package com.example.jc.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.example.jc.myapplication.model.HomeworkParent;
import com.example.jc.myapplication.model.MyEventDay;
import com.example.jc.myapplication.util.JsonUtilities;
import com.google.gson.Gson;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeworkActivity extends AppCompatActivity {

    Button mConfirmButton;
    CalendarView mCalendarView;
    ArrayList<EventDay> events;
    private Map<EventDay, List<HomeworkParent>> descriptionMap;
    Gson gson = new Gson();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework);

        mConfirmButton = findViewById(R.id.confirmButton);
        mCalendarView = findViewById(R.id.calendarView);

        SharedPreferences mPrefs = getSharedPreferences("onLoadStudent",MODE_PRIVATE);
        String json = mPrefs.getString("homework", "");



        Map<Date, ArrayList<HomeworkParent>> homework = JsonUtilities.parseHomeworkFromJson(json);

        events = new ArrayList<>();
        descriptionMap = new HashMap<>();

        for(Map.Entry<Date, ArrayList<HomeworkParent>> hw : homework.entrySet()){
            if(hw.getKey()!= null) {
                Calendar calendar = Calendar.getInstance();
//                String date = hw.getKey().toString();
//                int[] segment = DateUtilities.getYearMonthDay(date);
//                calendar.set(segment[0],segment[2]-1,segment[1]);
                calendar.setTime(hw.getKey());
                MyEventDay event = new MyEventDay(calendar,R.drawable.circle, gson.toJson(hw.getValue()));
                events.add(event);
                if(descriptionMap.containsKey(event)) {
                    List<HomeworkParent> homeworkParents = hw.getValue();
                    for (HomeworkParent h : homeworkParents) {
                        descriptionMap.get(event).add(h);
                    }
                }else{
                    descriptionMap.put(event,hw.getValue());
                }

            }
        }

        mCalendarView.setEvents(events);

        mCalendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                if(descriptionMap.containsKey(eventDay)){
                    Intent intent = new Intent(getApplicationContext(), HomeworkDetailActivity.class);
                    intent.putExtra("homework", gson.toJson(descriptionMap.get(eventDay)));
                    startActivity(intent);
                }
            }
        });
    }



    public void onClick(View view){

        Intent intent;

        switch (view.getId()){



            case R.id.confirmButton:
                intent = new Intent(this, HomeworkDetailActivity.class);
                intent.putExtra("type","teacher");
                startActivity(intent);
                break;

//            case R.id.attendanceButton:
//                intent = new Intent(this, AttendanceActivity.class);
//                intent.putExtra("type","admin");
//                break;

//            case R.id.announcementsButton:
//                intent = new Intent(this, AnnouncementsActivity.class);
//                intent.putExtra("type","admin");
//                break;

        }


    }
}
