package com.example.jc.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jc.myapplication.model.HomeworkParent;

import java.util.ArrayList;
import java.util.List;

public class MarksAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<List<String>> marks;
    private LayoutInflater mInflater;

    public MarksAdapter(Context context, ArrayList<List<String>> marks) {
        this.context = context;
        this.marks = marks;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override

    public int getCount() {
        return marks.size();
    }

    @Override
    public Object getItem(int position) {
        return marks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        List<String> mark = (List<String>) getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.marks_list_item, parent, false);
        }

        TextView subject = (TextView) convertView.findViewById(R.id.mark_subject);
        TextView description = (TextView) convertView.findViewById(R.id.mark_mark);

        subject.setText(mark.get(0));
        description.setText(mark.get(1));

        return convertView;
    }
}
