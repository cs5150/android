package com.example.jc.myapplication.model;

import java.io.Serializable;

/**
 * Created by apple on 3/3/18.
 */

public class Course implements Serializable {
    private int id;
    private Standard standard;
    private String subjectName;
    private Teacher teacher;


    public int getId() {
        return id;
    }

    public Standard getStandard() {
        return standard;
    }

    public void setStandard(Standard standard) {
        this.standard = standard;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
