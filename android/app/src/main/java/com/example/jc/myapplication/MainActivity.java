package com.example.jc.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.jc.myapplication.model.Homework;
import com.example.jc.myapplication.model.Student;
import com.example.jc.myapplication.util.JsonUtilities;
import com.example.jc.myapplication.util.NetworkUtilities;

import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Button mParentButton;
    Button mTeacherButton;
    Button mAdminButton;
    public static ArrayList<Student> studentList;
    public ArrayList<Homework> homeworkList;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Welcome!");
        getSharedPreferences("onLoadTeacher", 0).edit().clear().commit();
        getSharedPreferences("onLoadStudent", 0).edit().clear().commit();

        new ProcessJson().execute("");
        mParentButton = findViewById(R.id.parentButton);
        mTeacherButton = findViewById(R.id.teacherButton);
        mAdminButton = findViewById(R.id.adminButton);

    }

    public void onClick(View view){

        Intent intent = new Intent(this, LoginActivity.class);


        switch (view.getId()){

            case R.id.parentButton:
                intent.putExtra("type","parent");
                break;

            case R.id.teacherButton:
                intent.putExtra("type","teacher");
                break;

            case R.id.adminButton:
                intent.putExtra("type","admin");
                break;

        }

        startActivity(intent);
    }


    public class ProcessJson extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sharedPreferences = getSharedPreferences("shared", Context.MODE_PRIVATE);
        }

        @Override
        protected String doInBackground(String... params) {


//            URL url  = NetworkUtilities.buildURL("homework");
//
//            URL url2 = NetworkUtilities.buildURL("student");
//            String json = NetworkUtilities.getResponseFromHttp(url2);
//            String type = NetworkUtilities.getObjectType(url2);
//            ArrayList<Student> hi = JsonUtilities.parseJson(json,type);
//
//            String json2 = NetworkUtilities.getResponseFromHttp(url);
//            String type2 = NetworkUtilities.getObjectType(url);
//
//            Student student = hi.get(0);
//            URL url3  = NetworkUtilities.buildURL("absent/" + student.getId());
//            String json3 = NetworkUtilities.getResponseFromHttp(url3);
//            String type3 = NetworkUtilities.getObjectType(url3);
//            Log.e("tag", "doInBackground: " + json3);

//            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
//            prefsEditor.putString("hwJson", json2);
//            prefsEditor.putString("stJson", json);
//            prefsEditor.putString("abJson", json3);
//            prefsEditor.apply();
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}
