package com.example.jc.myapplication.util;

import android.content.SharedPreferences;

import com.example.jc.myapplication.model.Homework;
import com.example.jc.myapplication.model.HomeworkParent;
import com.example.jc.myapplication.model.OnLoadStudent;
import com.example.jc.myapplication.model.OnLoadTeacher;
import com.example.jc.myapplication.model.Student;
import com.example.jc.myapplication.model.Teacher;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by JC on 3/3/18.
 */

public final class JsonUtilities {

    public static ArrayList parseJson(String json, String type) {
        try {
            JSONArray parentArray = new JSONArray(json);
            Gson gson = new Gson();

            switch(type){
                case "student":
                    ArrayList<Student> studentList = new ArrayList<>();
                    for (int i = 0; i < parentArray.length(); i++) {
                        JSONObject child = parentArray.getJSONObject(i);
                        Student student = gson.fromJson(child.toString(), Student.class);
                        studentList.add(student);
                    }
                    return studentList;


                case "homework":
                    ArrayList<Homework> homeworkArrayList = new ArrayList<>();
                    for(int j=0; j<parentArray.length();j++){
                        JSONObject child = parentArray.getJSONObject(j);
                        Homework hw = gson.fromJson(child.toString(), Homework.class);
                        homeworkArrayList.add(hw);
                    }
                    return homeworkArrayList;

//                case "absent":
//                    ArrayList<AbsentDate> absentDatesList = new ArrayList<>();
//                    for(int j=0; j<parentArray.length();j++){
//                        JSONObject child = parentArray.getJSONObject(j);
//                        AbsentDate absentDates = gson.fromJson(child.toString(), AbsentDate.class);
//                        absentDatesList.add(absentDates);
//                    }
//                    return absentDatesList;

                default:
                    return null;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static Student saveStudentData(String json, SharedPreferences sharedPreferences) {

        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        OnLoadStudent onLoadStudent = gson.fromJson(json, OnLoadStudent.class);



        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("id", onLoadStudent.getId());
        prefsEditor.putString("standard", gson.toJson(onLoadStudent.getStandard()));
        prefsEditor.putString("subjectNames", gson.toJson(onLoadStudent.getSubjectNames()));
        prefsEditor.putString("homework", gson.toJson(onLoadStudent.getHomework()));
        prefsEditor.putString("marks", gson.toJson(onLoadStudent.getMarks()));
        prefsEditor.putString("announcements", gson.toJson(onLoadStudent.getAnnouncements()));
        prefsEditor.putString("student", gson.toJson(onLoadStudent.getStudent()));
        prefsEditor.apply();
        return onLoadStudent.getStudent();
    }

    public static Teacher saveTeacherData(String json, SharedPreferences sharedPreferences){

        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();

        OnLoadTeacher onLoadTeacher = gson.fromJson(json, OnLoadTeacher.class);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("teacherId", onLoadTeacher.getId());
        prefsEditor.putString("teacherMarks", gson.toJson(onLoadTeacher.getMarks()));
        prefsEditor.putString("teacherHomework", gson.toJson(onLoadTeacher.getHomework()));
        prefsEditor.putString("teacherAnnouncements", gson.toJson(onLoadTeacher.getAnnouncements()));
        prefsEditor.putString("teacher", gson.toJson(onLoadTeacher.getTeacher()));
        prefsEditor.apply();
        return onLoadTeacher.getTeacher();
    }


    public static Map<Date, ArrayList<HomeworkParent>> parseHomeworkFromJson(String json) {

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Type type = new TypeToken<Map<Date, ArrayList<HomeworkParent>>>(){}.getType();
        Map<Date, ArrayList<HomeworkParent>> homework = gson.fromJson(json, type);
        return homework;
    }

    public static OnLoadTeacher parseOnLoadTeacher(String json){
        Gson gson = new Gson();
        OnLoadTeacher teacher = gson.fromJson(json, OnLoadTeacher.class);
        return teacher;
    }


    public static ArrayList<HomeworkParent> parseHomeworkParentFromJson(String json) {

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<HomeworkParent>>(){}.getType();
        ArrayList<HomeworkParent> homework = gson.fromJson(json, type);
        return homework;
    }


    public static Map<Date, ArrayList<String>> parseAnnouncementFromJson(String json) {

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Type type = new TypeToken< Map<Date, ArrayList<String>>>(){}.getType();
        Map<Date, ArrayList<String>> announcements = gson.fromJson(json, type);
        return announcements;
    }

    public static Map<String, Map<String, Integer>> parseMarksFromJson (String json){
        Gson gson = new Gson();
        Type type = new TypeToken< Map<String, Map<String, Integer>>>(){}.getType();
        Map<String, Map<String, Integer>> parsedMarks = gson.fromJson(json, type);
        return parsedMarks;

    }

    public static void saveTeacherData(String json, String type) {

    }

}
