package com.example.jc.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by apple on 3/3/18.
 */

public class StandardInfo implements Serializable {
    private ArrayList<Course> courses;
    private int id;
    private Standard standard;
    private ArrayList<Student> students;

    public StandardInfo(ArrayList<Course> courses, int id, Standard standard, ArrayList<Student> students) {
        this.courses = courses;
        this.id = id;
        this.standard = standard;
        this.students = students;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Standard getStandard() {
        return standard;
    }

    public void setStandard(Standard standard) {
        this.standard = standard;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }
}
