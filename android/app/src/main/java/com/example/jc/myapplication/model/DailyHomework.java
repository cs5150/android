package com.example.jc.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by apple on 3/3/18.
 */

public class DailyHomework implements Serializable {
    private String dateOfAnnounceMent;
    private ArrayList<HomeworkDescriptor> homework;
    int id;

    public DailyHomework(String dateOfAnnounceMent, ArrayList<HomeworkDescriptor> homework, int id) {
        this.dateOfAnnounceMent = dateOfAnnounceMent;
        this.homework = homework;
        this.id = id;
    }

    public String getDateOfAnnounceMent() {
        return dateOfAnnounceMent;
    }

    public void setDateOfAnnounceMent(String dateOfAnnounceMent) {
        this.dateOfAnnounceMent = dateOfAnnounceMent;
    }

    public ArrayList<HomeworkDescriptor> getHomework() {
        return homework;
    }

    public void setHomework(ArrayList<HomeworkDescriptor> homework) {
        this.homework = homework;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
