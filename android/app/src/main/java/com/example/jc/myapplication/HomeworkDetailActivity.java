package com.example.jc.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.example.jc.myapplication.model.HomeworkParent;
import com.example.jc.myapplication.util.JsonUtilities;

import java.util.ArrayList;
import java.util.Map;

public class HomeworkDetailActivity extends AppCompatActivity {
    private ListView mListView;

    ArrayList<HomeworkParent> homeworkParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework_detail);
        setTitle("Homework Detail");
        Intent intent = new Intent(this, HomeworkActivity.class);
        String json = getIntent().getExtras().getString("homework");
        mListView = (ListView) findViewById(R.id.homework_list_view);

        homeworkParent = JsonUtilities.parseHomeworkParentFromJson(json);
        final HomeworkAdapter adapter = new HomeworkAdapter(this, homeworkParent);

        mListView.setAdapter(adapter);

    }
}
