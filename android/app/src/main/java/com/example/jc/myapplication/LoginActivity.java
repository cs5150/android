package com.example.jc.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    EditText mlogin;
    EditText mPassword;
    Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");
        mLoginButton = findViewById(R.id.loginButton);
        mlogin = findViewById(R.id.loginField);
        mPassword = findViewById(R.id.passwordField);

    }

    public void onClick(View view){
        //to profile page
        String user = getIntent().getExtras().getString("type");
        switch (user){

            case "parent":
                Intent parentIntent = new Intent(this, ParentActivity.class);
                parentIntent.putExtra("stdAdNum", mlogin.getText().toString());
                startActivity(parentIntent);
                break;

            case "teacher":
                Intent teacherIntent = new Intent(this, TeacherActivity.class);
                teacherIntent.putExtra("teacherAdNum",mlogin.getText().toString());
                startActivity(teacherIntent);
                break;

            case "admin":
                Intent adminIntent = new Intent(this, TeacherActivity.class);
                adminIntent.putExtra("type","admin");
                startActivity(adminIntent);
                break;

        }


//        for(Student stu : MainActivity.studentList){
//            if(Integer.toString(stu.getRegisterNo()).equals(mlogin.getText())){
//                //check password too
//                startActivity(intent);
//            }
//        }

    }
}
