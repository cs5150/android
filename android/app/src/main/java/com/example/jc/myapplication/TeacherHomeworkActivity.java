package com.example.jc.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.applandeo.materialcalendarview.EventDay;
import com.example.jc.myapplication.model.Homework;
import com.example.jc.myapplication.model.HomeworkParent;
import com.example.jc.myapplication.model.OnLoadTeacher;
import com.example.jc.myapplication.model.Teacher;
import com.example.jc.myapplication.util.JsonUtilities;
import com.example.jc.myapplication.util.NetworkUtilities;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jingqi on 4/13/18.
 */

public class TeacherHomeworkActivity extends AppCompatActivity {
    Map<String, ArrayList<HomeworkParent>> outer;
    ArrayList<String> names;
    String course;
    Teacher teacher;
    Map<String, Map<String, ArrayList<HomeworkParent>>> homework;
    Map<String, ArrayList<String>> announcements;
    String teacherId;
    Map<String, Map<String, Map<String, Integer>>> marks;
    Intent intent;

    ArrayList<Pair<String, String>> dataPairs;
    private TeacherHomeworkAdapter teacherhwadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_post_hw_list);
        ListView hwList = (ListView) findViewById(R.id.postHw_list);
        intent = getIntent();
        loadPreferences();

        Map<String, ArrayList<HomeworkParent>> outer = homework.get(intent.getStringExtra("class"));

        dataPairs = new ArrayList<>();
        for (String s : outer.keySet()) {
            for (HomeworkParent hw : outer.get(s)) {
                dataPairs.add(new Pair<String, String>(s, hw.getDescription()));
                System.out.println(s + " " + hw.getDescription());
            }
        }

        teacherhwadapter = new TeacherHomeworkAdapter(this, dataPairs);
        hwList.setAdapter(teacherhwadapter);

        Button postButton = (Button) findViewById(R.id.postHw_button);
        postButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent postHw = new Intent(getApplicationContext(), AddHomeworkActivity.class);
                postHw.putExtra("class", intent.getStringExtra("class"));
                startActivityForResult(postHw, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 1) {
            if (data.hasExtra("isEdit")){
                int pos = data.getIntExtra("pos", -1);
                Pair<String, String> pair = dataPairs.get(pos);

                Map<String, ArrayList<HomeworkParent>> outer = homework.get(intent.getStringExtra("class"));
                ArrayList<HomeworkParent> hwListforDay = outer.get(pair.first);
                for (HomeworkParent hwParent : hwListforDay){
                    if (hwParent.getDescription() == pair.second){
                        hwParent.setDescription(data.getStringExtra("desc"));

                        new ProcessJson().execute("");
                    }
                }

                pair = new Pair<String, String>(data.getStringExtra("date"), data.getStringExtra("desc"));
                teacherhwadapter.notifyDataSetChanged();

            }
            else if (data.hasExtra("date") && data.hasExtra("desc")) {
                dataPairs.add(new Pair<String, String>(data.getStringExtra("date"), data.getStringExtra("desc")));
                teacherhwadapter.notifyDataSetChanged();
            }
        }
    }

    public void loadPreferences() {
        SharedPreferences pref = getSharedPreferences("onLoadTeacher", MODE_PRIVATE);
        String teacherMarks = pref.getString("teacherMarks", "");
        String teacherHomework = pref.getString("teacherHomework","");
        String teacherAnnouncements = pref.getString("teacherAnnouncements","");
        String teacherJson = pref.getString("teacher","");

        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, Map<String, Map<String, Integer>>>>(){}.getType();
        Type type2 = new TypeToken<Map<String, Map<String, ArrayList<HomeworkParent>>>>(){}.getType();
        Type type3 = new TypeToken<Map<String, ArrayList<String>>>(){}.getType();
        teacherId = pref.getString("teacherId","");
        marks = gson.fromJson(teacherMarks, type);
        homework = gson.fromJson(teacherHomework,type2);
        announcements = gson.fromJson(teacherAnnouncements,type3);
        teacher = gson.fromJson(teacherJson,Teacher.class);
    }

    public class ProcessJson extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = NetworkUtilities.buildURL("load/teacher/"+teacherId);
            OnLoadTeacher onLoadTeacher = new OnLoadTeacher();
            onLoadTeacher.setId(teacherId);
            onLoadTeacher.setMarks(marks);
            onLoadTeacher.setAnnouncements(announcements);
            onLoadTeacher.setTeacher(teacher);
            onLoadTeacher.setHomework(homework);
            boolean what = NetworkUtilities.postMarks(url, onLoadTeacher);
            Log.i("test",""+what);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    public void removeHw(int position){
        Pair<String, String> hwInfo = dataPairs.get(position);
        Map<String, ArrayList<HomeworkParent>> outer = homework.get(intent.getStringExtra("class"));
        System.out.println(outer.toString());
        System.out.println(homework);

        ArrayList<HomeworkParent> hwListforDay = outer.get(hwInfo.first);
        for (HomeworkParent hwParent : hwListforDay){
            if (hwParent.getDescription() == hwInfo.second){
                hwListforDay.remove(hwParent);
                if(hwListforDay.size() == 0){
                    outer.remove(hwInfo.first);
                }
            }
        }

        System.out.println(outer.toString());
        System.out.println(homework);

        dataPairs.remove(position);

        teacherhwadapter.notifyDataSetChanged();

        new ProcessJson().execute("");
    }

    public void editHw(int position){
        Pair<String, String> hwInfo = dataPairs.get(position);

        Intent postHw = new Intent(getApplicationContext(), AddHomeworkActivity.class);
        postHw.putExtra("class", intent.getStringExtra("class"));
        postHw.putExtra("date", hwInfo.first);
        postHw.putExtra("desc", hwInfo.second);
        postHw.putExtra("isEdit", true);
        postHw.putExtra("pos", position);
        startActivityForResult(postHw, 1);
    }
}
