package com.example.jc.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ExamTypeActivity extends AppCompatActivity {
    Button midterm1;
    Button quarterly;
    Button midterm2;
    Button halfyear;
    Button midterm3;
    Button annual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_type);

        midterm1 = findViewById(R.id.midterm1);
        quarterly = findViewById(R.id.quarterly);
        midterm2 = findViewById(R.id.midterm2);
        halfyear = findViewById(R.id.halfyear);
        midterm3 = findViewById(R.id.midterm3);
        annual = findViewById(R.id.annual);




    }

    public void onClick(View view){
        int id = view.getId();
        Intent intent;
        switch(id){
            case R.id.midterm1:
                intent = new Intent(getApplicationContext(), TeacherExam.class);
                intent.putExtra("examType", "MIDTERM 1");
                startActivity(intent);
                break;
            case R.id.midterm2:
                intent = new Intent(getApplicationContext(), TeacherExam.class);
                intent.putExtra("examType", "MIDTERM 2");
                startActivity(intent);
                break;
            case R.id.midterm3:
                intent = new Intent(getApplicationContext(), TeacherExam.class);
                intent.putExtra("examType", "MIDTERM 3");
                startActivity(intent);
                break;
            case R.id.quarterly:
                intent = new Intent(getApplicationContext(), TeacherExam.class);
                intent.putExtra("examType", "QUARTERLY");
                startActivity(intent);
                break;
            case R.id.halfyear:
                intent = new Intent(getApplicationContext(), TeacherExam.class);
                intent.putExtra("examType", "HALF YEARLY");
                startActivity(intent);
                break;
            case R.id.annual:
                intent = new Intent(getApplicationContext(), TeacherExam.class);
                intent.putExtra("examType", "ANNUAL");
                startActivity(intent);
                break;


        }


    }
}
