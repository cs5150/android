package com.example.jc.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jc.myapplication.model.Announcement;

import java.util.ArrayList;

public class AnnouncementAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Announcement> announcements;
    private LayoutInflater mInflater;


    public AnnouncementAdapter(Context context, ArrayList<Announcement> announcements) {
        this.context = context;
        this.announcements = announcements;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override

    public int getCount() {
        return announcements.size();
    }

    @Override
    public Object getItem(int position) {
        return announcements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Announcement announcement = (Announcement) getItem(position);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.announcement_list_item, parent, false);
        }

        TextView eventDate = (TextView) convertView.findViewById(R.id.eventDate);

        eventDate.setText(announcement.getDate().toString());

        ArrayList<String> currentItem = announcement.getDescription();

        AnnouncementDesAdapter adapter = new AnnouncementDesAdapter(context, currentItem);
//        eventDescription.setText(announcement.getDescription().toString());
        ListView listView = (ListView) convertView.findViewById(R.id.descriptionList);
        listView.setAdapter(adapter);

        return convertView;
    }
}
