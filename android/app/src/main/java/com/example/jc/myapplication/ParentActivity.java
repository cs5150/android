package com.example.jc.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jc.myapplication.model.OnLoadTeacher;
import com.example.jc.myapplication.model.Student;
import com.example.jc.myapplication.util.JsonUtilities;
import com.example.jc.myapplication.util.NetworkUtilities;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.URL;

public class ParentActivity extends AppCompatActivity {

    Button mHomeworkButton;
    Button mMarksButton;
    Button mAnnouncementsButton;
    TextView nameText;
    TextView numberText;
    SharedPreferences sharedPreferences;
    String stdAdNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);

        mHomeworkButton = findViewById(R.id.homeworkButton);
        mMarksButton = findViewById(R.id.marksButton);
        mAnnouncementsButton = findViewById(R.id.announcementsButton);
        nameText = findViewById(R.id.nameText);
        numberText = findViewById(R.id.numberText);
        sharedPreferences = getSharedPreferences("onLoadStudent", Context.MODE_PRIVATE);
        stdAdNum = getIntent().getExtras().getString("stdAdNum");

        new ParentActivity.ProcessJson().execute("");


    }

    public void onClick(View view){

        Intent intent;

        switch (view.getId()){

            case R.id.homeworkButton:

                intent = new Intent(this, HomeworkActivity.class);
                startActivity(intent);
                break;

            case R.id.marksButton:

                intent = new Intent(this, ChooseMarks.class);
                startActivity(intent);

                break;


            case R.id.announcementsButton:
                intent = new Intent(this, AnnouncementActivity.class);
                startActivity(intent);
                break;

        }
    }

    public class ProcessJson extends AsyncTask<String, Void, String> {
        private String json;
        private OnLoadTeacher onLoadTeacher;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            URL url  = NetworkUtilities.buildURL("load/student/" + stdAdNum);
            json = NetworkUtilities.getResponseFromHttp(url);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Student student = JsonUtilities.saveStudentData(json, sharedPreferences);
            setTit(student);
            Log.i("test",student.getName());

            super.onPostExecute(s);
        }
    }

    public boolean setTit(Student student) {
        if (student == null) {
            return false;
        }
        nameText.setText(student.getName());
        numberText.setText(stdAdNum);
        return true;
    }

}
